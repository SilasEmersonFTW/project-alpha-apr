from django import forms


# if you see this changed from Accounts to Login,
# I misunderstood the feature and had named it AccountForm
# which probably might dock me points but IM JUST SAYING I
# FIXED IT HAVE MERCY
class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )


class SignupForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )
    password_confirmation = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )
